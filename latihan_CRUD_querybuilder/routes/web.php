<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/index', function () {
    return view('welcome');
});


Route::get('/data-table', function (){
    return view('halaman.data-table');
});

Route::get('/cast', 'app\Http\Controllers\CastController@index');
Route::get('/cast/create', 'app\Http\Controllers\CastController@create');
Route::post('/cast', 'app\Http\Controllers\CastController@store');
Route::get('/cast/{cast_id}', 'app\Http\Controllers\CastController@show');
Route::get('/cast/{cast_id}/edit', 'app\Http\Controllers\CastController@edit');
Route::put('/cast/{cast_id}', 'app\Http\Controllers\CastController@update');
Route::delete('/cast/{cast_id}', 'app\Http\Controllers\CastController@destroy');